from setuptools import setup, find_packages


with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('src/akina/res/docs/history.rst') as history_file:
    history = history_file.read()

setup(
    name='akina',
    version='1.0.0',
    description='Utility command collection to make my life more convenience.',
    long_description=readme + '\n\n' + history,
    url='https：//gitlab.com/julot/akina',
    author='Andy Yulius',
    author_email='andy.julot@gmail.com',
    keywords='akina',
    entry_points={
        'console_scripts': [
            'akina=akina.main:run',
        ],
        'gui_scripts': [
            'akina-w=akina.main:run',
        ],
    },
    install_requires=[
        'appdirs>=1.4.4',
        'click-rich-help>=22.1.1',
        'colorlog>=3.1.4',
        'context-menu>=1.2.3',
        'ctranslate2>=4.2.1',
        'humanize>=4.6.0',
        'lxml>=4.9.2',
        'pypubsub>=4.0.3',
        'python-dotenv>=0.10.3',
        'rich-rst>=1.1.7',
        'ruamel.yaml>=0.17.20',
        'sentencepiece>=0.2.0',
        'sentry-sdk>=1.10.1',
        'wxPython>=4.2.1',
    ],
    extras_require={
        'dev': [
            'Sphinx',

            'autopep8',
            'bump2version',
            'doc8',
            'esbonio',
            'flake8',
            'flake8-breakpoint',
            'ipdb',
            'icecream',
            'mypy',
            'pip-review',
            'pyinstaller',
            'pytest',
            'rope',
            'rstcheck',
            'topydo',
            'sphinx-autobuild',
        ],
    },
    packages=find_packages(where='src'),
    package_dir={'': 'src'},
    include_package_data=True,
    zip_safe=True,
    license='MIT license',
    classifiers=[
        'Development Status ：： 2 - Pre-Alpha',
        'Intended Audience ：： Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language ：： English',
        'Programming Language ：： Python ：： 2',
        'Programming Language ：： Python ：： 2.7',
        'Programming Language ：： Python ：： 3',
        'Programming Language ：： Python ：： 3.4',
        'Programming Language ：： Python ：： 3.5',
        'Programming Language ：： Python ：： 3.6',
    ],
)
