===============
Shinozaki Akina
===============

|PyPI version| |PyPI pyversions| |PyPI license| |PyPI status| |PyPI format|
|Travis| |Read The Docs|

.. |PyPI version| image:: https://img.shields.io/pypi/v/akina.svg
   :target: https://pypi.org/project/akina

.. |PyPI pyversions| image:: https://img.shields.io/pypi/pyversions/akina.svg
   :target: https://pypi.org/project/akina

.. |PyPI license| image:: https://img.shields.io/pypi/l/akina.svg
   :target: https://pypi.org/project/akina

.. |PyPI status| image:: https://img.shields.io/pypi/status/akina.svg
   :target: https://pypi.org/project/akina

.. |PyPI format| image:: https://img.shields.io/pypi/format/akina.svg
   :target: https://pypi.org/project/akina

.. |Travis| image:: https://img.shields.io/travis/julot/akina.svg
        :target: https://travis-ci.org/julot/akina

.. |Read The Docs| image:: https://readthedocs.org/projects/akina/badge/?version=latest
        :target: https://akina.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


Utility command collection to make my life more convenience.


* Free software: MIT license



Installation
============

To install Shinozaki Akina, run this command in your terminal:

.. code-block::

   > python -m pip install akina


This is the preferred method to install Shinozaki Akina,
as it will always install the most recent stable release.

If you don't have `pip`_ installed,
this `Python installation guide`_ can guide you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


Usage
=====

.. code-block:: bat

   > python -m pip install akina
   > akina --help

Full documentation is `here`_.

.. _here: https://akina.readthedocs.io.
