@echo off
setlocal enabledelayedexpansion

pushd %~dp0

rem Command file for Shinozaki Akina

if exist build/ rm -rf build/
rm -f *.spec

if "%1" == "" goto help
if "%1" == "exe" goto compile
if "%1" == "cli" goto compile
if "%1" == "gui" goto compile
if "%1" == "wheel" goto wheel
if "%1" == "l10n" goto l10n


:help
echo [31mShinozaki Akina v1.0.0[0m
echo.Please specify what to make:
echo.  [94mwheel       [0mto build wheel distribution
echo.  [94mcli         [0mto build windows executable with console
echo.  [94mgui         [0mto build windows executable without console
echo.  [94mexe         [0mto build both windows executable
echo.  [94ml10n        [0mto build localization language
goto end


:compile
if not "%2" == "--yes" (
	echo.
	echo. [101;93m                                                                              [0m
	echo. [101;93m                         * * *   W A R N I N G   * * *                        [0m
	echo. [101;93m                                                                              [0m
	echo.
	echo.Due to limitation in bumpversion, you must change version in
	echo."res\version.ffi" manually.
	echo.
	set /p continue="[93mDo you want to continue?[0m [y/N]: "
	if /i "!continue!" neq "Y" goto end
)

if exist src\akina\locale call :localization

if "%1" == "exe" (
	call :compile console Akina
	call :compile windowed Akina-W
)
if "%1" == "cli" call :compile console Akina
if "%1" == "gui" call :compile windowed Akina-W
goto end


:compile
pyinstaller ^
	--onefile ^
	--%1 ^
	--name=%2 ^
	--icon=src/akina/res/icons/akina.ico ^
	--hidden-import=colorlog ^
	--version-file=src\akina\res\.ffi ^
	--add-data=src\akina\res\.env;.\akina\res\ ^
	--add-data=src\akina\res\.yml;.\akina\res\ ^
	--add-data=src\akina\res\docs\authors.rst;.\akina\res\docs\ ^
	--add-data=src\akina\res\docs\history.rst;.\akina\res\docs\ ^
	--add-data=src\akina\locale\id\LC_MESSAGES\akina.mo;.\locale\id\LC_MESSAGES\ ^
	src/akina/main.py
goto :eof

:wheel
python setup.py bdist_wheel
goto end


:l10n
if not exist src\akina\locale mkdir src\akina\locale
pybabel ^
    extract ^
    --keywords=t ^
    --project="Shinozaki Akina" ^
    --version="1.0.0" ^
    --copyright-holder="Andy Yulius" ^
    --msgid-bugs-address="andy.julot@gmail.com" ^
    --output-file=src\akina\locale\.pot ^
    src\akina
if exist src\akina\locale\id\LC_MESSAGES\akina.po (
    pybabel update -i src\akina\locale\.pot -D akina -d src\akina\locale
) else (
    pybabel init -i src\akina\locale\.pot -D akina -d src\akina\locale -l id
)
call :localization
goto end


:localization
pybabel compile -D akina -d src\akina\locale
goto :eof


:end
if exist build/ rm -rf build/
rm -f *.spec
popd
