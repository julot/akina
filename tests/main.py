import click.testing

import akina.commands


def test():
    runner = click.testing.CliRunner()
    help_result = runner.invoke(akina.commands.group, ['--help'])
    assert help_result.exit_code == 0
    assert 'Show this message and exit.' in help_result.output
