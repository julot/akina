import importlib.resources
import logging
import pathlib
import sys
import typing

import click
import ruamel.yaml

import akina
import akina.res
import akina.win32.message


def get_default() -> pathlib.Path:
    with importlib.resources.path(akina.res, '.yml') as path:
        return path


def get_instance() -> pathlib.Path:
    path = pathlib.Path(akina.dirs.user_config_dir)
    return path.joinpath('.yml')


def get_instance_or_default() -> pathlib.Path:
    instance = get_instance()
    return instance if instance.exists() else get_default()


def load(path: typing.Optional[pathlib.Path] = None) -> dict:
    """
    Returns instance config if PATH is None.

    Raises click.Abort when PATH is defined but missing.

    """
    log = logging.getLogger(__name__)

    if path is None:
        path = get_instance()

    if not path.exists():
        msg = f'Config file "{path}" is missing.'
        log.error(msg)

        if sys.stdout.isatty():
            akina.console.print(msg, style='info')
            akina.console.print(
                'Type [info]akina --help[/] for more information.'
            )
        else:
            akina.win32.message.show(
                '\n'.join([
                    msg,
                    '',
                    'Please run the CLI version for more information.',
                    '',
                    '',
                    '',
                    akina.__copyright__,
                ]),
                akina.__title__,
                akina.win32.message.ICON_INFORMATION,
            )

        raise click.Abort()

    log.info(f'Loading configuration from "{path}" file.')
    return ruamel.yaml.YAML(typ='safe').load(path)
