import logging

import click
import click_rich_help
import context_menu.menus

import akina
import akina.commands.folder.iconize
import akina.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme=akina.rich.theme,
    short_help=(
        'Add context menu to File Explorer (Windows) or Nautilus (Linux).'
    ),
)
def command() -> None:
    """
    Add context menu to File Explorer (Windows) or Nautilus (Linux).

    """
    log = logging.getLogger(__name__)
    log.info('Akina->Register command started.')
    run()
    log.info('Akina->Register command finished.')


def show(filenames, _):
    akina.commands.folder.iconize.run(filenames[0], index=0, hide=False)


def hide(filenames, _):
    akina.commands.folder.iconize.run(filenames[0], index=0, hide=True)


def run() -> None:
    menu = context_menu.menus.ContextMenu('Iconize this folder', type='FILES')
    shown = context_menu.menus.ContextCommand(
        "Don't hide icon file",
        python=show,
    )
    hidden = context_menu.menus.ContextCommand(
        'Hide icon file',
        python=hide,
    )
    menu.add_items([shown, hidden])
    menu.compile()
