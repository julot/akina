import click
import click_rich_help

import akina
import akina.rich

from . import (
    normalize as __normalize,
)


@click.group(
    cls=click_rich_help.StyledGroup,
    theme=akina.rich.theme,
    short_help='Miscelenoeus CG operation.',
)
def group() -> None:
    """
    Miscelenoeus CG operation.

    """


group.add_command(__normalize.command, name='normalize')
