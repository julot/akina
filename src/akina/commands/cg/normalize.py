import logging
import pathlib
import string

import click
import click_rich_help

import akina
import akina.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme=akina.rich.theme,
    short_help='Normalize CG filename.',
)
@click.argument(
    'path',
    type=click.Path(
        exists=True,
        file_okay=False,
        dir_okay=True,
        path_type=pathlib.Path,
    ),
)
def command(path: pathlib.Path):
    """
    Normalize CG collection in PATH by renaming all file in the sub directory
    into "SUBDIRNAME_[A-Z].ext", primarily used for Ren'Py.

    """
    log = logging.getLogger(__name__)
    log.info('Akina->CG->Normalize command started.')
    run(path)
    log.info('Akina->CG->Normalize command finished.')


def run(path: pathlib.Path):
    for sub in path.iterdir():
        if sub.is_dir():
            process_dir(sub)


def process_dir(path: pathlib.Path):
    files = (x for x in path.iterdir() if x.is_file())
    for file, index in zip(files, string.ascii_lowercase):
        new_name = f'{path.stem}_{index}{file.suffix}'
        new_path = pathlib.Path(path, new_name)
        file.rename(new_path)
