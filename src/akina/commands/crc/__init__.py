import logging
import pathlib
import sys

import click
import click_rich_help

import akina
import akina.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme=akina.rich.theme,
    short_help='Cyclic redundancy check for file.',
)
@click.argument(
    'paths',
    metavar='[PATH]...',
    nargs=-1,
    type=click.Path(
        exists=True,
        file_okay=True,
        dir_okay=True,
        writable=True,
        readable=True,
        allow_dash=True,
        resolve_path=True,
        path_type=pathlib.Path,
    ),
)
@click.option(
    '-g',
    '--gui',
    'windowed',
    is_flag=True,
    help='Show the GUI version.',
    default=False,
)
def command(paths: list[pathlib.Path], windowed: bool):
    """
    Check one or more files against the available checksum in filename or add
    the checksum to filename if not exists using CRC-32 algorithm.

    The checksum format is 8 digit hexadecimal inside square bracket [FFFFFF].

    """
    log = logging.getLogger(__name__)
    log.info('Akina->CRC command started.')

    paths = flatten_paths(paths)
    log.info(f'Selected files: {paths}')

    if (not windowed) and sys.stdout and sys.stdout.isatty():
        log.info('Activating CLI version.')
        from . import cli

        cli.run(paths)
    else:
        log.info('Activating GUI version.')
        from . import gui

        gui.run(paths)

    log.info('Akina->CRC command finished.')


def listdir(path: pathlib.Path) -> list[pathlib.Path]:
    log = logging.getLogger(__name__)
    files = []
    for each in path.iterdir():
        if each.is_file():
            log.debug('"%s" is file. Appended to list.', each)
            files.append(each)
        else:
            log.debug('"%s" is not a file. Skip.', each)

    return files


def flatten_paths(paths: list[pathlib.Path]) -> list[pathlib.Path]:
    """
    Flatten paths into list of files.

    """
    log = logging.getLogger(__name__)
    files = []
    for path in paths:
        if path.is_file():
            log.debug('"%s" is file. Appended to list.', path)
            files.append(path)
        else:
            log.debug(
                '"%s" is directory. Extend list to directory content.',
                path,
            )
            files.extend(listdir(path))

    return files
