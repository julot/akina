import click

from . import util


def run(paths):
    fail = (
        '[danger]Checksum fail[/]. '
        'Expected: [warning]{}[/]. '
        'Calculated: [warning]{}[/].'
    )

    for path in paths:
        existing_checksum = util.pluck_checksum(path.name)
        print(f'[primary]{path.name}[/]')

        size = path.stat().st_size
        bar = click.progressbar(
            length=size,
            label='Checking',
            width=61,
            show_eta=False,
        )
        checksum = util.check(path, gauge_callback=bar.update)
        click.echo()

        if existing_checksum:
            if existing_checksum == checksum:
                print('[success]Checksum OK.[/]')
            elif existing_checksum.upper() == checksum:
                print('[warning]Existing checksum not in upper case.[/]')
                util.replace_checksum(path, existing_checksum, checksum)
                print('[info]Checksum changed to upper case.[/]')
            else:
                print(fail.format(existing_checksum, checksum))
                click.pause()
        else:
            util.append_checksum(path, checksum)
            print('[info]Checksum added to filename.[/]')
