import logging
import threading

import humanize
import wx

import akina
import akina.commands.crc.util
import akina.config

from pubsub import pub


class Presenter(object):

    def __init__(self, paths, view, interactor):
        self.view = view
        interactor.install(self, view)
        self.populate_files(paths)
        self.view.gauge.total.start(sum(path.stat().st_size for path in paths))
        self.set_size()
        self.subscribe()
        self.auto_close = bool(paths)
        view.Show()
        view.Refresh()

        self.stopped = threading.Event()
        self.ran = threading.Event()
        self.ran.set()

        self.thread = Thread(paths, self.ran, self.stopped)
        self.thread.start()

    def subscribe(self):
        pub.subscribe(self.start_check, 'start_check')
        pub.subscribe(self.end_check, 'end_check')
        pub.subscribe(self.update_gauge, 'gauge')
        pub.subscribe(self.finish, 'finish')

    def finish(self):
        if self.auto_close:
            self.view.Close()

    def start_check(self, index, path):
        log = logging.getLogger(__name__)
        log.debug('Start CRC.')

        self.view.files.SetItemImage(index, self.view.files.images['control'])
        self.view.gauge.file.start(path.stat().st_size)

    def end_check(self, path, checksum, index):
        log = logging.getLogger(__name__)
        log.debug('End CRC.')

        files = self.view.files

        existing = files.GetItemText(index, 2)

        if not existing:
            akina.commands.crc.util.append_checksum(path, checksum)
            files.SetItemTextColour(index, wx.Colour(191, 181, 0))
            files.SetItem(index, 4, 'Checksum added')
            files.SetItemImage(index, self.view.files.images['tick'])
        elif existing == checksum:
            files.SetItemTextColour(index, wx.Colour(19, 133, 53))
            files.SetItem(index, 4, 'Checksum OK')
            files.SetItemImage(index, self.view.files.images['tick'])
        elif existing.upper() == checksum:
            akina.commands.crc.util.replace_checksum(path, existing, checksum)
            files.SetItemTextColour(index, wx.Colour(0, 162, 232))
            files.SetItem(index, 4, 'Checksum changed to upper case')
            files.SetItemImage(index, self.view.files.images['tick'])
        else:
            files.SetItemTextColour(index, wx.Colour(237, 28, 36))
            files.SetItem(index, 4, 'Checksum fail')
            files.SetItemImage(index, self.view.files.images['cross'])
            self.auto_close = False

        files.SetItem(index, 3, checksum)

        self.view.gauge.file.finish()

    def update_gauge(self, value):
        self.view.gauge.file.update(value)
        self.view.gauge.total.update(value)

    def set_size(self):
        config = akina.config.load(akina.config.get_instance_or_default())

        x = config['crc']['gui'].get('x', -1)
        y = config['crc']['gui'].get('y', -1)
        width = config['crc']['gui'].get('width', 800)
        height = config['crc']['gui'].get('height', 300)

        self.view.SetSize(x, y, width, height)
        if x == -1 or y == -1:
            self.view.Center()

    def teardown(self):
        self.stop()
        # self.save_size_and_position()

    def save_size_and_position(self):
        # TODO Save setting to yaml with https://pypi.org/project/ruamel.yaml
        # Ruamel preserve comment and order
        # config = akina.config.load(akina.config.get_instance_or_default())

        log = logging.getLogger(__name__)
        # (x, y) = self.view.GetPosition()
        # (w, h) = self.view.GetSize()
        # if x0 != x or y0 != y or w0 != w or h0 != h:
        #     config['crc']['gui']['x'] = x
        #     config['crc']['gui']['y'] = y
        #     config['crc']['gui']['w'] = w
        #     config['crc']['gui']['h'] = h
        #     config.save()
        #     log.debug('Size and position saved for future use.')
        # else:
        log.info("Size and position didn't change.")

    def populate_files(self, paths):
        files = self.view.files
        image = files.images['clock']

        for path in paths:
            index = files.InsertItem(files.GetItemCount(), path.name, image)

            files.SetItem(index, column=5, label=str(path.parent))
            files.SetItem(
                index,
                column=1,
                label=humanize.naturalsize(path.stat().st_size, binary=True),
            )

            crc = akina.commands.crc.util.pluck_checksum(path.name)
            if crc:
                files.SetItem(index, column=2, label=crc)

    def stop(self):
        self.ran.set()
        self.stopped.set()

        self.view.pause.Disable()
        self.view.stop.Disable()

    def pause(self):
        if self.ran.is_set():
            self.ran.clear()
            self.view.pause.SetLabel('Continue')
        else:
            self.ran.set()
            self.view.pause.SetLabel('Pause')


class Thread(threading.Thread):

    def __init__(self, paths, ran, stopped):
        super().__init__()
        self.paths = paths
        self.ran = ran
        self.stopped = stopped

    def run(self):
        for index, path in enumerate(self.paths):
            self.ran.wait()

            pub.sendMessage('start_check', index=index, path=path)

            checksum = akina.commands.crc.util.check(
                path,
                gauge_callback=self.update_gauge,
                ran_event=self.ran,
                stop_event=self.stopped,
            )

            if self.stopped.is_set():
                break
            else:
                pub.sendMessage(
                    'end_check',
                    path=path,
                    checksum=checksum,
                    index=index,
                )
        else:
            pub.sendMessage('finish')

    def update_gauge(self, value):
        pub.sendMessage('gauge', value=value)
