import importlib.resources

import wx

import akina.res.icons
import akina.res.images.toolbars


class View(wx.Frame):

    def __init__(self):
        super().__init__(
            None,
            title='Shinozaki Akina: Cyclic Redundancy Check',
        )
        parent = wx.Panel(self)
        self.files = Files(parent)
        self.gauge = GaugeSizer(parent)
        self.pause = Pause(parent)
        self.stop = Stop(parent)

        self.__set_icon()
        self.__arrange()
        parent.Refresh()

    def __set_icon(self):
        with importlib.resources.path(akina.res.icons, 'akina.ico') as path:
            icon = wx.IconBundle(str(path), type=wx.BITMAP_TYPE_ICO)
            self.SetIcons(icon)

    def __arrange(self):
        flags = wx.SizerFlags

        button_sizer = wx.BoxSizer(wx.HORIZONTAL)
        button_sizer.Add(self.pause, flags().Expand())
        button_sizer.Add(self.stop, flags().Expand())

        gauge_sizer = wx.BoxSizer(wx.HORIZONTAL)
        gauge_sizer.Add(self.gauge, flags().Proportion(1).Expand())
        gauge_sizer.Add(button_sizer, flags().Expand().Border(wx.LEFT))

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(
            self.files,
            flags().Proportion(1).Expand().Border(wx.ALL),
        )
        sizer.Add(
            gauge_sizer,
            flags().Expand().Border(wx.ALL ^ wx.TOP),
        )

        self.pause.GetParent().SetSizer(sizer)


class Files(wx.ListCtrl):

    def __init__(self, parent):
        super().__init__(parent, style=wx.LC_REPORT)
        self.__set_image_list()
        self.__set_columns()

    def __set_image_list(self):
        self.image_list = wx.ImageList(width=16, height=16)
        self.images = {}

        for each in ['clock', 'control', 'control_pause', 'cross', 'tick']:
            with importlib.resources.path(
                akina.res.images.toolbars,
                f'{each}.png',
            ) as path:
                image = wx.Image(str(path), type=wx.BITMAP_TYPE_PNG)
                self.images[each] = self.image_list.Add(wx.Bitmap(image))

        self.SetImageList(self.image_list, which=wx.IMAGE_LIST_SMALL)

    def __set_columns(self):
        columns = [
            ('Filename', wx.LIST_FORMAT_LEFT, 300),
            ('Size', wx.LIST_FORMAT_RIGHT, 70),
            ('Saved Checksum', wx.LIST_FORMAT_CENTER, 102),
            ('Checksum', wx.LIST_FORMAT_CENTER, 102),
            ('Status', wx.LIST_FORMAT_LEFT, 200),
            ('Location', wx.LIST_FORMAT_LEFT, 1000),
        ]
        for index, (heading, format, width) in enumerate(columns):
            self.InsertColumn(index, heading, format, width)


class GaugeSizer(wx.FlexGridSizer):

    def __init__(self, parent):
        flags = wx.SizerFlags()

        gap = flags.GetDefaultBorder()
        super().__init__(cols=2, vgap=gap, hgap=gap)
        self.AddGrowableCol(1)

        text = wx.StaticText(parent, label='File:')
        self.Add(text, flags=flags.Right())

        self.file = Gauge(parent)
        self.Add(self.file, flags=flags.Expand())

        text = wx.StaticText(parent, label='Total:')
        self.Add(text, flags=flags.Right())

        self.total = Gauge(parent)
        self.Add(self.total, flags=flags.Expand())


class Gauge(wx.Gauge):

    def __init__(self, parent):
        super().__init__(parent)
        self.percent = 0
        self.buffer = 0
        self.Range = 100

    def update(self, value):
        """
        Update gauge by value incrementally.

        """
        self.buffer += value
        self.Value = int(max(0, min(self.buffer / self.total * 100, 100)))
        self.Refresh()

    def start(self, total):
        """
        Start gauge.

        """
        self.total = total
        self.buffer = 0
        self.Value = 0

    def finish(self):
        """
        Set gauge bar to 100%.

        """
        self.Value = self.Range


class Pause(wx.Button):

    def __init__(self, parent):
        super().__init__(parent, label='Pause')
        with importlib.resources.path(
            akina.res.images.toolbars,
            'control_pause.png',
        ) as path:
            image = wx.Image(str(path), type=wx.BITMAP_TYPE_PNG)
            self.SetBitmap(wx.Bitmap(image))


class Stop(wx.Button):

    def __init__(self, parent):
        super().__init__(parent, label='Stop')
        with importlib.resources.path(
            akina.res.images.toolbars,
            'control_stop_square.png',
        ) as path:
            image = wx.Image(str(path), type=wx.BITMAP_TYPE_PNG)
            self.SetBitmap(wx.Bitmap(image))
