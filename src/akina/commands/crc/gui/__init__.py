import wx

from ._view import View
from ._presenter import Presenter
from ._interactor import Interactor


def run(paths):
    app = wx.App()
    view = View()
    interactor = Interactor()
    Presenter(paths, view, interactor)
    app.MainLoop()
