import logging

import wx


class Interactor(object):

    def install(self, presenter, view):
        self.presenter = presenter
        self.view = view
        self.view.Bind(wx.EVT_CLOSE, self.on_close)
        self.view.stop.Bind(wx.EVT_BUTTON, self.on_stop)
        self.view.pause.Bind(wx.EVT_BUTTON, self.on_pause)

    def on_close(self, event):
        self.presenter.teardown()
        event.Skip()

    def on_stop(self, event):
        log = logging.getLogger(__name__)
        log.debug('Stop clicked.')

        self.presenter.stop()
        event.Skip()

    def on_pause(self, event):
        log = logging.getLogger(__name__)
        log.debug('Pause clicked.')

        self.presenter.pause()
        event.Skip()
