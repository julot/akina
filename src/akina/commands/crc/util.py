import re
import zlib


checksum_pattern = re.compile(r'\[([\da-f]{8})\]', re.RegexFlag.IGNORECASE)


def pluck_checksum(string):
    """
    Pluck checksum from filename in format [FFFFFFFF].

    """
    r = re.findall(checksum_pattern, string)
    return r[-1] if r else None


def check(path, gauge_callback, ran_event=None, stop_event=None):
    crc = 0
    buffer = int(max(4_194_304, min(path.stat().st_size // 100, 8_388_608)))

    with open(path, "rb") as f:
        while True:
            if ran_event:
                ran_event.wait()

            # Reading in a chunk of the data
            data = f.read(buffer)

            if not data:
                break

            gauge_callback(len(data))
            crc = zlib.crc32(data, crc)

            if stop_event and stop_event.is_set():
                break

        # If the crc hex value is negative,
        # bitwise and it with the maximum 32bit value.
        # Apparently this is a 'bit mask' resulting in a 32bit long value
        # (rather than an infinitely long value,
        # see http://stackoverflow.com/a/7825412).
        # It is also guaranteed to return a  positive number
        if crc < 0:
            crc &= 2 ** 32 - 1

    if stop_event and stop_event.is_set():
        return ''

    return '%.8X' % crc


def append_checksum(path, checksum):
    dst = '{} [{}]{}'.format(path.stem, checksum, path.suffix)
    path.rename(path.parent.joinpath(dst))


def replace_checksum(path, old, new):
    path.rename(str(path).replace(old, new))
