import logging

import click
import click_rich_help

import akina
import akina.config
import akina.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme=akina.rich.theme,
    short_help='Send error log to [primary]sentry.io[/] for testing purpose.',
)
def command() -> None:
    """
    Send error log to [primary]sentry.io[/] to test whether sentry is working
    or not.

    Please make sure that sentry is [success]enabled[/] in config

    """
    log = logging.getLogger(__name__)
    log.info('Akina->Sentry command started.')
    run()
    log.info('Akina->Sentry command finished.')


def run() -> None:
    config = akina.config.load(akina.config.get_instance_or_default())

    if not config['sentry']['enabled']:
        print('Sentry is [danger]disabled[/].')
        print(
            'Please [success]enable[/] sentry in [info].yml[/] configuration '
            'file.'
        )
        return

    log = logging.getLogger(__name__)
    log.error(
        'Error log example send from '
        f'{akina.__name__}  Version {akina.__version__}'
    )
