import click
import click_rich_help

import akina
import akina.rich

from . import (
    gamelist as __gamelist,
)


@click.group(
    cls=click_rich_help.StyledGroup,
    theme=akina.rich.theme,
    short_help='RecalBox tools.',
)
def group():
    """
    Collection of command to work with RecalBox.

    """


group.add_command(__gamelist.group, name='gamelist')
