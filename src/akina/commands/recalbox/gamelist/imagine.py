import codecs
import contextlib
import logging
import lxml.etree
import os
import pathlib
import xml.etree.ElementTree
import xml.dom.minidom

import click
import click_rich_help

import akina
import akina.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme=akina.rich.theme,
    short_help='Add image element to game list.',
)
@click.argument(
    'path',
    type=click.Path(
        exists=True,
        file_okay=True,
        dir_okay=False,
        writable=False,
        readable=True,
        allow_dash=True,
        path_type=pathlib.Path,
    ),
)
def command(path):
    """
    Add image element to MAME gamelist.xml file created by Skraper
    (http://skraper.net).

    This command simply add image element with the value using <filename>.png
    without even checking whether the file exists or not.

    I created this because for some reason Skraper didn't create image element
    even though I download the image.
    Maybe because I use custom config for media.

    """
    log = logging.getLogger(__name__)
    log.info('Akina->RecalBox->GameList->Imagine command started.')
    run(path)
    log.info('Akina->RecalBox->GameList->Imagine command finished.')


def run(path):
    log = logging.getLogger(__name__)

    target = 'K:/test.xml'
    with contextlib.suppress(FileNotFoundError):
        os.remove(target)

    tree = xml.etree.ElementTree.parse(path)
    root = tree.getroot()
    for child in root:
        if child.tag != 'game':
            continue

        image_path = get_image_path(child.find('path').text)

        image = xml.etree.ElementTree.Element('image')
        image.text = image_path
        child.append(image)

    data = xml.etree.ElementTree.tostring(root, 'utf-8')
    root = lxml.etree.fromstring(data)
    content = lxml.etree.tostring(root, pretty_print=True, encoding=str)
    log.debug(f'{type(content)}')
    with codecs.open(target, 'w', 'utf-8') as f:
        f.write(content)


def get_image_path(path):
    basename = os.path.basename(path)
    name = os.path.splitext(basename)[0]
    return f'media/images/{name}.png'
