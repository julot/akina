import click
import click_rich_help

import akina
import akina.rich

from . import (
    imagine as __imagine,
)


@click.group(
    cls=click_rich_help.StyledGroup,
    theme=akina.rich.theme,
    short_help='RecalBox game list tools.',
)
def group():
    """
    Collection of command to work with RecalBox game list.

    """


group.add_command(__imagine.command, name='imagine')
