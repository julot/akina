import logging
import shutil

import click
import click_rich_help

import akina
import akina.config
import akina.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme=akina.rich.theme,
    short_help='Initialize akina.',
)
@click.confirmation_option(
    prompt='Overwrite existing configuration file?',
)
def command() -> None:
    """
    Create configuration file [primary].yml[/] and [primary].env[/] with
    default value.

    """
    log = logging.getLogger(__name__)
    log.info('Akina->Init command started.')
    run()
    log.info('Akina->Init command finished.')


def run() -> None:
    dst = akina.config.get_instance()
    dst.parent.mkdir(parents=True, exist_ok=True)
    shutil.copy(akina.config.get_default(), dst)
    print(f'Configuration file [info].yml[/] created at [info]{dst}[/]')
