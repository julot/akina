import logging

import click
import click_rich_help
import context_menu.menus

import akina
import akina.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme=akina.rich.theme,
    short_help=(
        'Remove context menu from File Explorer (Windows) or Nautilus (Linux).'
    ),
)
def command() -> None:
    """
    Remove context menu from File Explorer (Windows) or Nautilus (Linux).

    """
    log = logging.getLogger(__name__)
    log.info('Akina->Unregister command started.')
    run()
    log.info('Akina->Unregister command finished.')


def run() -> None:
    context_menu.menus.removeMenu('Iconize this folder', type='FILES')
