import importlib.resources
import logging

import click
import click_rich_help
import rich_rst

import akina
import akina.res.docs
import akina.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme=akina.rich.theme,
    short_help='See version history.',
)
def command() -> None:
    """
    See version history.

    """
    log = logging.getLogger(__name__)
    log.info('Akina->History command started.')
    run()
    log.info('Akina->History command finished.')


def run() -> None:
    with importlib.resources.path(akina.res.docs, 'history.rst') as path:
        with open(path) as f:
            akina.console.print(rich_rst.RestructuredText(f.read()))
