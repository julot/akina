import logging
import os
import pathlib
import platform

import click
import click_rich_help
import configparser

import akina
import akina.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme=akina.rich.theme,
    short_help='Set folder icon.',
)
@click.argument(
    'path',
    type=click.Path(
        exists=True,
        file_okay=True,
        writable=True,
        readable=True,
        resolve_path=True,
        dir_okay=False,
    ),
)
@click.option(
    '--index',
    '-i',
    default=0,
    type=click.IntRange(min=0),
    help='Icon index. Default to 0. ',
)
@click.option(
    '--hide',
    '-h',
    is_flag=True,
    help='Hide the icon file. File will be renamed to .ico',
)
def command(path: str, index: int, hide: bool):
    """
    Iconize the folder where the icon resides.

    \b
    Note:
    Only works in Windows due to Win32API call.

    """
    log = logging.getLogger(__name__)
    log.info('Akina->Folder->Iconize command started.')
    run(path, index, hide)
    log.info('Akina->Folder->Iconize command finished.')


def run(path: str, index: int, hide: bool):
    if platform.system() == 'Windows':
        import ctypes
        import stat

    icon = pathlib.Path(path)

    if hide and not icon.suffix == '':
        dst = pathlib.Path(icon.parent, icon.suffix)
        if dst.exists():
            dst.unlink()
        icon = icon.rename(dst)

    config = configparser.ConfigParser()

    ini = pathlib.Path(icon.parent, 'desktop.ini')
    if not ini.exists():
        ini.touch()

    config.read(ini)

    config['.ShellClassInfo'] = {
        'IconResource': f'{icon.name},{index}',
        'IconFile': icon.name,
        'IconIndex':  str(index),
    }

    if platform.system() == 'Windows':
        ctypes.windll.kernel32.SetFileAttributesW(
            str(ini),
            stat.FILE_ATTRIBUTE_NORMAL,
        )

    with ini.open('w') as f:
        config.write(f)

    if platform.system() == 'Windows':
        attribute = stat.FILE_ATTRIBUTE_HIDDEN + stat.FILE_ATTRIBUTE_SYSTEM
        ctypes.windll.kernel32.SetFileAttributesW(str(ini), attribute)

        if hide:
            ctypes.windll.kernel32.SetFileAttributesW(str(icon), attribute)

    os.chmod(icon.parent, stat.S_IREAD)
