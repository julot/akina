import logging
import os
import stat

import click
import click_rich_help

import akina
import akina.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme=akina.rich.theme,
    short_help='Delete cache file.',
)
@click.argument(
    'path',
    type=click.Path(
        exists=True,
        file_okay=False,
        writable=True,
        readable=True,
        resolve_path=True,
        dir_okay=True,
    ),
)
def command(path):
    """
    Delete cache file used by Windows (Thumbs.db) from PATH recursively.

    """
    log = logging.getLogger(__name__)
    log.info('Akina->Folder->Clean command started.')
    run(path)
    log.info('Akina->Folder->Clean command finished.')


def run(path):
    cache_files = ['thumbs.db']
    msg = 'Found cache file [warning]{}[/] in [warning]{}[/].'
    for root, _, files in os.walk(path, topdown=False):
        print(f'Checking [primary]{root}[/] folder.')
        for name in files:
            if name.lower() in cache_files:
                print(msg.format(name, root))
                print('[primary]Attempting to delete...[/]', end=' ')
                filename = os.path.join(root, name)
                os.chmod(filename, stat.S_IWUSR)
                try:
                    os.remove(filename)
                except RuntimeError as err:
                    print('[danger]FAILED[/].')
                    print(str(err))
                else:
                    print('[success]OK[/].')
