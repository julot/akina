import click
import click_rich_help

import akina
import akina.rich

from . import (
    iconize as __iconize,
    clean as __clean,
)


@click.group(
    cls=click_rich_help.StyledGroup,
    theme=akina.rich.theme,
    short_help='Do something to folder.',
)
def group() -> None:
    """
    Do something to folder.

    """


group.add_command(__iconize.command, name='iconize')
group.add_command(__clean.command, name='clean')
