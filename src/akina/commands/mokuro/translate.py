import csv
import json
import logging
import pathlib
import typing

import click
import click_rich_help
import ctranslate2
import sentencepiece

import akina
import akina.rich


class Sugoi(object):

    def __init__(
        self,
        model: pathlib.Path,
        tokenizator: pathlib.Path,
        substitution: pathlib.Path,
    ) -> None:
        self._model_path = model
        self._tokenizator_path = tokenizator
        self._tokenizator = None
        self._translator = None
        self._substitution_path = substitution
        self._substitutions = None

    @staticmethod
    def load_lines(path: pathlib.Path) -> typing.List[str]:
        """
        Returns lines from Mokuro JSON file.

        """
        with path.open(encoding='utf-8') as f:
            data = json.load(f)

        result = []
        for block in data['blocks']:
            line = ''.join(block['lines']).strip()
            if line:
                result.append(line)

        return result

    @property
    def translator(self) -> ctranslate2.Translator:
        if self._translator is None:
            self._translator = ctranslate2.Translator(
                str(self._model_path),
                device='cpu',
            )

        return self._translator

    @property
    def tokenizator(self) -> sentencepiece.SentencePieceProcessor:
        if self._tokenizator is None:
            self._tokenizator = sentencepiece.SentencePieceProcessor(
                model_file=str(self._tokenizator_path),
            )

        return self._tokenizator

    def translate(self, path: pathlib.Path):
        print(f'Translating {path.name}:')
        result = []
        lines = self.load_lines(path)
        total = len(lines)
        for index, line in enumerate(lines):
            print(f'    Balloon {index + 1} of {total}... ', end='')

            result.append(line)
            result.append('')

            if self._substitution_path is not None:
                for subs in self.substitutions:
                    line = line.replace(subs[0], subs[1])

            tokenized_text = self.tokenizator.encode(
                [line],
                out_type=str,
                enable_sampling=True,
                alpha=0.1,
                nbest_size=-1,
            )
            tokenized_translated = self.translator.translate_batch(
                tokenized_text
            )

            translated_text = [
                ''.join(text.hypotheses[0])
                for text
                in tokenized_translated
            ]

            for translated_line in translated_text:
                result.append(
                    translated_line
                    .replace('▁', ' ')
                    .replace("@", ".")
                    .strip()
                )
                result.append('')
                result.append('')

            print('done.')

        en = pathlib.Path(path.parent, f'{path.stem}.txt')
        with en.open(mode='w', encoding='utf-8') as f:
            f.writelines('\n'.join(result))
        print(f'    Saved as {en.name}')

    @property
    def substitutions(self):
        if self._substitutions is None:
            self._substitutions = []
            with self._substitution_path.open(encoding='utf-8') as f:
                csv_reader = csv.reader(f, delimiter=',')
                for row in csv_reader:
                    if row:
                        self._substitutions.append(row)

        return self._substitutions


@click.command(
    cls=click_rich_help.StyledCommand,
    theme=akina.rich.theme,
    short_help='Translate Mokuro dir or file.',
)
@click.argument(
    'path',
    type=click.Path(
        exists=True,
        file_okay=True,
        dir_okay=True,
        writable=True,
        readable=True,
        allow_dash=True,
        resolve_path=True,
        path_type=pathlib.Path,
    ),
)
@click.option(
    '-m',
    '--model',
    prompt='Sugoi model directory path',
    help='Path to Sugoi model directory.',
    type=click.Path(
        exists=True,
        file_okay=False,
        dir_okay=True,
        readable=True,
        allow_dash=True,
        resolve_path=True,
        path_type=pathlib.Path,
    ),
)
@click.option(
    '-t',
    '--tokenizator',
    prompt='Sugoi tokenizator path',
    help='Path to Sugoi tokenizator.',
    type=click.Path(
        exists=True,
        file_okay=True,
        dir_okay=False,
        readable=True,
        allow_dash=True,
        resolve_path=True,
        path_type=pathlib.Path,
    ),
)
@click.option(
    '-s',
    '--substitution',
    help='Path to substitution csv file in "<old>,<new>" format.',
    default=None,
    type=click.Path(
        exists=True,
        file_okay=True,
        dir_okay=False,
        readable=True,
        allow_dash=True,
        resolve_path=True,
        path_type=pathlib.Path,
    ),
)
def command(
    path: pathlib.Path,
    model: pathlib.Path,
    tokenizator: pathlib.Path,
    substitution: pathlib.Path,
) -> None:
    """
    Translate JSON file or directory produced by Mokuro.

    """
    log = logging.getLogger(__name__)
    log.info('Akina->Mokuro->Translate command started.')
    run(path, model, tokenizator, substitution)
    log.info('Akina->Mokuro->Translate command finished.')


def run(
    path: pathlib.Path,
    model: pathlib.Path,
    tokenizator: pathlib.Path,
    substitution: pathlib.Path,
) -> None:
    sugoi = Sugoi(model, tokenizator, substitution)
    if path.is_file():
        sugoi.translate(path)
    else:
        for p in path.iterdir():
            if p.suffix == '.json':
                en = pathlib.Path(path.parent, f'{path.stem}.en.txt')
                if not en.exists():
                    sugoi.translate(p)
