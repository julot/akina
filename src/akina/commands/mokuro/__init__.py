import click
import click_rich_help

import akina
import akina.rich

from . import (
    translate as __translate,
)


@click.group(
    cls=click_rich_help.StyledGroup,
    theme=akina.rich.theme,
    short_help='Mokuro operation.',
)
def group() -> None:
    """
    Mokuro operation.

    For more information about Mokuro, go to
    https://github.com/kha-white/mokuro

    """


group.add_command(__translate.command, name='translate')
