import click
import click_rich_help

import akina
import akina.rich

from . import (
    copy as __copy,
)


@click.group(
    cls=click_rich_help.StyledGroup,
    theme=akina.rich.theme,
    short_help='Miscelenoeus MAME operation.',
)
def group() -> None:
    """
    Miscelenoeus MAME operation.

    """


group.add_command(__copy.command, name='copy')
