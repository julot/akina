import contextlib
import logging
import pathlib
import shutil

import click
import click_rich_help

import akina
import akina.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme=akina.rich.theme,
    short_help='Copy MAME game file.',
)
@click.argument(
    'ini',
    type=click.Path(
        exists=True,
        file_okay=True,
        dir_okay=False,
        path_type=pathlib.Path,
    ),
)
@click.argument(
    'source',
    type=click.Path(
        exists=True,
        file_okay=False,
        dir_okay=True,
        path_type=pathlib.Path,
    ),
)
@click.argument(
    'destination',
    type=click.Path(
        exists=True,
        file_okay=False,
        dir_okay=True,
        writable=True,
        path_type=pathlib.Path,
    ),
)
def command(
    ini: pathlib.Path,
    source: pathlib.Path,
    destination: pathlib.Path,
):
    """
    Copy MAME game file (.zip) according to INI file from SOURCE folder to
    DESTINATION folder.

    Using this command avoid us to copy all game of MAME.

    \b
    You cat get INI files from a website such as:
    https://www.progettosnaps.net/support/

    """
    log = logging.getLogger(__name__)
    log.info('Akina->MAME->Copy command started.')
    run(ini, source, destination)
    log.info('Akina->MAME->Copy command finished.')


def run(ini: pathlib.Path, source: pathlib.Path, destination: pathlib.Path):
    categories = load_category(ini)
    for index, (key, value) in enumerate(categories.items(), start=1):
        akina.console.print(
            f'[warning]{index:>3,}[/]. {key} [info]({len(value):,})[/]',
            highlight=False,
        )

    print()
    prompt = click.prompt('Which category do yo wish to copy?', type=int)
    category = list(categories.keys())[prompt - 1]
    games = categories[category]
    akina.console.print(
        'You choose category: '
        f'[warning]{category}[/] containing [info]{len(games):,}[/] games.',
        highlight=False,
    )

    if not click.confirm('Do you want to continue?'):
        return

    copy(games, source, destination)


def load_category(path: pathlib.Path) -> dict[str, list[str]]:
    categories = {}  # type: dict[str, list[str]]
    with path.open() as f:
        for line in f.readlines():
            line = line.strip()
            if not line:
                continue

            if line.startswith('['):
                category = line.strip()[1:-1]  # type: str
                if category.isupper():
                    continue

                categories[category] = []
                continue

            with contextlib.suppress(KeyError):
                categories[category].append(line)

    return categories


def copy(games: list[str], source: pathlib.Path, target: pathlib.Path):
    counters = {'missing': 0, 'success': 0, 'skipped': 0, 'overwritten': 0}
    for game in games:
        src = source.joinpath(f'{game}.zip')
        dst = target.joinpath(f'{game}.zip')
        akina.console.print(f'Copying {src.name}...', end=' ', highlight=False)

        if not src.exists():
            counters['missing'] = counters['missing'] + 1
            print('[danger]Error[/]. [info]File not exists.')
            continue

        if dst.exists():
            if dst.stat().st_mtime >= src.stat().st_mtime:
                counters['skipped'] = counters['skipped'] + 1
                print('[info]Skipped. Already exists and the same.')
                continue

            counters['overwritten'] = counters['overwritten'] + 1
            print('[info]Overwritten. Already exists but older.')
        else:
            counters['success'] = counters['success'] + 1
            print('[success]OK.')

        try:
            shutil.copy2(src, dst)
        except Exception as exc:
            print(f'[danger]Error[/]. [warning]{exc}')
            click.Abort()

    akina.console.print(
        (
            'Copy operation done.\n'
            f'- [success]Success     : {counters["success"]:,}[/]\n'
            f'- [warning]Missing     : {counters["missing"]:,}[/]\n'
            f'- [primary]Skipped     : {counters["skipped"]:,}[/]\n'
            f'- [secondary]Overwritten : {counters["overwritten"]:,}[/]\n'
        ),
        end='',
        highlight=False
    )
