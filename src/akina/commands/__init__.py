import logging
import sys

import click
import click_rich_help

import akina
import akina.config
import akina.rich
import akina.win32.message

from . import (
    authors as __authors,
    crc as __crc,
    cg as __cg,
    folder as __folder,
    # foo as __foo,
    history as __history,
    init as __init,
    mame as __mame,
    mokuro as __mokuro,
    recalbox as __recalbox,
    register as __register,
    sentry as __sentry,
    unregister as __unregister,
)


@click.group(
    cls=click_rich_help.StyledGroup,
    theme=akina.rich.theme,
    # invoke_without_command=True,
)
@click.version_option(version=akina.__version__, message='%(version)s')
@click.pass_context
def group(ctx) -> None:
    """
    \b
    [warning]Shinozaki Akina[/]
    [primary]Version 1.0.0[/primary]

    Utility command collection to make my life more convenience.

    [success]© 2023 Andy Yulius[/]

    """
    if ctx.invoked_subcommand is None:
        log = logging.getLogger(__name__)
        log.info('Akina started.')
        run()
        log.info('Akina stopped.')


group.add_command(__authors.command, name='authors')
group.add_command(__crc.command, name='crc')
group.add_command(__cg.group, name='cg')
group.add_command(__folder.group, name='folder')
# group.add_command(__foo.command, name='foo')
group.add_command(__history.command, name='history')
group.add_command(__init.command, name='init')
group.add_command(__mame.group, name='mame')
group.add_command(__mokuro.group, name='mokuro')
group.add_command(__recalbox.group, name='recalbox')
group.add_command(__register.command, name='register')
group.add_command(__sentry.command, name='sentry')
group.add_command(__unregister.command, name='unregister')


def run() -> None:
    # config = akina.config.load()

    msg = 'Akina invoked without sub command.\n'
    if sys.stdout.isatty():
        # Entry point is console_scripts
        akina.rich.console.print(msg, style='info')
        akina.rich.console.print(
            'Type [warning]akina --help[/] for more information.'
        )
    else:
        # Entry point is gui_scripts, headless python (without console)
        akina.win32.message.show(
            '\n'.join([
                msg,
                '',
                'Please run the CLI version for more information.',
                '',
                '',
                '',
                akina.__copyright__,
            ]),
            akina.__title__,
            akina.win32.message.ICON_INFORMATION,
        )
