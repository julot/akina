import builtins
import logging
import logging.config

import akina
import akina.commands
import akina.config


def run():
    config = akina.config.load(akina.config.get_instance_or_default())
    logging.config.dictConfig(config['logging'])

    log = logging.getLogger(__name__)
    if not config['sentry']['enabled']:
        log.warning('Sentry is disabled.')
    else:
        log.info('Sentry is enabled.')

        import sentry_sdk
        import sentry_sdk.integrations.logging

        sentry_logging = sentry_sdk.integrations.logging.LoggingIntegration(
            level=logging.ERROR,  # Capture info and above as breadcrumbs
            event_level=logging.ERROR,  # Send errors as events
        )
        sentry_sdk.init(
            dsn=config['sentry']['dsn'],
            integrations=[
                sentry_logging,
            ],
            traces_sample_rate=config['sentry']['traces_sample_rate'],
        )

    try:
        import icecream

        icecream.install()
        log.info(
            'IceCream installed to built-ins successfully. '
            'ic command is now available globally.'
        )

    except ImportError:  # Graceful fallback if IceCream isn't installed.
        builtins.ic = lambda *a: None if not a else (a[0] if len(a) == 1 else a)  # noqa
        log.info(
            'IceCream module is not exists. '
            'ic command will fallback to produce nothing.'
        )

    del config

    akina.commands.group()


if __name__ == "__main__":
    run()
