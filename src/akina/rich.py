import typing

import rich.console
import rich.theme


# Color list: https://rich.readthedocs.io/en/stable/appendix/colors.html
theme = rich.theme.Theme({
    'primary': 'bright_blue',
    'secondary': 'blue',
    'success': 'green',
    'danger': 'bright_red',
    'warning': 'yellow1',
    'info': 'bright_cyan',
    'muted': 'bright_black',

    'header': 'bold italic cyan',
    'option': 'bold yellow',
    'metavar': 'green',
    'default': 'dim',
    'required': 'dim red',
})


console = rich.console.Console(theme=theme)


def print(
    *objects: typing.Any,
    sep: str = " ",
    end: str = "\n",
    file: typing.Optional[typing.IO[str]] = None,
    flush: bool = False,
) -> None:
    _console = (
        console
        if file is None
        else rich.console.Console(file=file, theme=theme)
    )
    return _console.print(*objects, sep=sep, end=end)
