# Because I like to extend 3rd party library by creating module/package here,
# all imported 3rd party library here should be prefixed with underscore.

import contextlib as _contextlib
import gettext as _gettext
import locale as _locale
import os as _os
import pathlib as _pathlib
import sys as _sys

import appdirs as _appdirs
import dotenv as _dotenv


__title__ = 'Shinozaki Akina'
__version__ = '1.0.0'
__description__ = (
    'Utility command collection to make my life more convenience.'
)
__author__ = 'Andy Yulius'
__email__ = 'andy.julot@gmail.com'
__copyright__ = '© 2019-2020 Andy Yulius'


frozen = getattr(_sys, 'frozen', False)
dirs = _appdirs.AppDirs('Akina', "Julot")


# Save path to current exe (pyInstaller) directory or current working directory
path = (
    _pathlib.Path(_sys.executable).parent if frozen
    else _pathlib.Path(_os.getcwd())
)


# Load environment variables
_dotenv.load_dotenv(dotenv_path=path.joinpath('.env') if frozen else None)


# Set language and load translation
_locale.setlocale(_locale.LC_ALL, _os.environ.get('LANGUAGE', 'en'))

_localedir = _os.path.join(
    _os.path.abspath(_os.path.dirname(__file__)),
    'locale',
)

# ! For executable produced by PyInstaller
# * Change _localedir if _MEIPASS attribute exists
with _contextlib.suppress(AttributeError):
    _localedir = _os.path.join(getattr(_sys, '_MEIPASS'), 'locale')

translation = _gettext.translation(
    'akina',
    _localedir,
    fallback=True,
    languages=[_os.environ.get('LANGUAGE', 'en')],
)
